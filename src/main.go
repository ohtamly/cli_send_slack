package main

import (
	"errors"
	"flag"
	"fmt"
	"ohtamly/send_slack"
	"os"
)

// .env で SEND_SLACK_INCOMING_WEBHOOK_URL がセットされていること
func main() {
	flag.Parse()

	// 引数が少ない or help ならヘルプメッセージを表示して終了する
	if len(flag.Args()) < 1 || flag.Arg(0) == "help" {
		help()
		os.Exit(statusHelp)
	}

	// メイン
	message := flag.Arg(0)
	incomingWebhookURL := os.Getenv("SEND_SLACK_INCOMING_WEBHOOK_URL")

	if err := validate(message, incomingWebhookURL); err != nil {
		panic(err)
	}

	slack := send_slack.SendSlack{
		IncomingWebhookURL: incomingWebhookURL,
	}
	if err := slack.Send(message); err != nil {
		panic(err)
	}
}

// ステータスコード
const statusHelp = 1

// ヘルプメッセージを表示する
func help() {
	println(`
Slackにメッセージをサクッと送信します。

[Usage]
	send_slack <message>|help

[Example]
	send_slack "コマンドが使えるようになったよ！"
`)
}

// 最低文字数
const validateMinMessageLength = 5

// 操作ミスによる異常送信をしないようにメッセージを検証する
func validate(message, incomingWebhookURL string) error {
	if len(incomingWebhookURL) == 0 {
		return errors.New("SEND_SLACK_INCOMING_WEBHOOK_URL が設定されていません。")
	}
	if len(message) < validateMinMessageLength {
		return fmt.Errorf("文字数が少なすぎるのため送信しませんでした: %d文字 (最低 %d文字)", len(message), validateMinMessageLength)
	}
	return nil
}
