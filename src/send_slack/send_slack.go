package send_slack

import (
	"bytes"
	"encoding/json"
	"net/http"
)

// SendSlack コンストラクター
type SendSlack struct {
	IncomingWebhookURL string
}

// Send メッセージを送信する
func (s SendSlack) Send(message string) error {
	data := map[string]interface{}{
		"text": message,
	}
	payload, err := json.Marshal(data)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(
		"POST",
		s.IncomingWebhookURL,
		bytes.NewBuffer(payload),
	)
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	return err
}
