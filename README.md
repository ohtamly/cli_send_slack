# send_slack
サクっとSlackにメッセージを送りたいときのためのコマンドです。

WindowsでもMacでもLinuxでも使いたいので、シェルスクリプトではなくあえてGoで作ってます。

## 開発環境
- macOS 11.1+
- Go 1.15+

## ビルド方法

```sh
$ cd src
$ go build -o ../bin/send_slack main.go
$ chmod +x ../bin/send_slack
$ ../bin/send_slack # Hello World!
```

## 使い方 (予定)

```sh
$ export SEND_SLACK_XXXXX_TARGET_USER_ID=123456789 # XXXXX時のメンション先 (XXXXXは任意)
$ export SEND_SLACK_INCOMING_WEBHOOK_URL=https://hogehoge/hogehoge/hogehoe # Slackのアプリ設定にあるIncoming WebhooksにあるURL
$ send_slack "コマンドが使えるようになったよ！"
```